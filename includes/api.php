<?
class Base {
	public 	$debug			= false;

	public function pr($str, $force = false) {
		if (!$this->debug && !$force) return false;

		echo '<pre>';
			print_r($str);
		echo '</pre>';
	}
}

class Api extends Base {
	public 	$lang			= 'en'; // en, fr, de, es
	private $verify 		= false;
	private $certPath		= '';
	private $apiUrl			= 'https://api.guildwars2.com/v1/';
	private $urls 			= array(
								'worldnames' 	=> 'world_names.json',
								'mapnames'		=> 'map_names.json',
								'eventnames'	=> 'event_names.json',
								'events'		=> 'events.json',
								'matches'		=> 'wvw/matches.json',
								'matchdetails' 	=> 'wvw/match_details.json',
								'objectivenames'=> 'wvw/objective_names.json',
								'items'			=> 'items.json',
								'itemdetails'	=> 'item_details.json',
								'recipes'		=> 'recipes.json',
								'recipedetails'	=> 'recipe_details.json',
								'guilddetails'	=> 'guild_details.json',
								'buildid'		=> 'build.json'
								);

	public function api($debug = false, $verify = true, $certPath = '') {
		$this->verify 		= $verify;
		$this->debug 		= $debug;
		$this->certPath 	= ($certPath != '') ? $certPath : dirname(__FILE__) . DIRECTORY_SEPARATOR . 'certificates' . DIRECTORY_SEPARATOR . 'cacert.pem';
	}

	private function getData($url = '', $debug = false) {
		if (!$url) return false;
		$ch = curl_init($this->apiUrl . $url);

		if ($this->verify) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_CAINFO, $this->certPath);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data 	= curl_exec($ch);

		if (curl_error($ch)) {
			$this->pr(curl_error($ch));
			return false;
		}

		$this->pr(json_decode($data, true));

		return json_decode($data, true);
	}

	/* -- General -- */

	public function getWorldnames() {
		return $this->getData($this->urls['worldnames'] . '?lang=' . $this->lang);
	}

	public function getMapnames() {
		return $this->getData($this->urls['mapnames'] . '?lang=' . $this->lang);
	}

	public function getEventnames() {
		return $this->getData($this->urls['eventnames'] . '?lang=' . $this->lang);
	}

	public function getEventStatus($worldId = null, $mapId = null, $eventId = null) {
		return $this->getData(	$this->urls['events'] . 
								($worldId || $mapId || $eventId ? '?' : '') .
								($worldId ? 'world_id=' . $worldId . ($mapId || $eventId ? '&' : '') : '') .
								($mapId ? 'map_id=' . $mapId . ($eventId ? '&' : '') : '') .
								($eventId ? 'event_id='. $eventId : '')
							);
	}

	public function getBuildId() {
		return $this->getData($this->urls['buildid']);
	}

	/* -- WvWvW --*/

	public function getMatches() {
		return $this->getData($this->urls['matches']);
	}

	public function getMatchDetails($matchId = null) {
		return $this->getData($this->urls['matchdetails'] . '?match_id=' . ($matchId ? $matchId : ''));
	}

	public function getObjectivenames() {
		return $this->getData($this->urls['objectivenames'] . '?lang=' . $this->lang);
	}

	/* -- Items -- */

	public function getItems() { // heavy function
		return $this->getData($this->urls['items']);
	}

	public function getItemDetails($itemId = null) {
		return $this->getData($this->urls['itemdetails'] . '?item_id=' . ($itemId ? $itemId : '') . '&lang=' . $this->lang);
	}

	/* -- Recipes -- */

	public function getRecipes() {
		return $this->getData($this->urls['recipes']);
	}

	public function getRecipeDetails($recipeId = null) {
		return $this->getData($this->urls['recipedetails'] . '?recipe_id=' . ($recipeId ? $recipeId : '') . '&lang=' . $this->lang);
	}

	/* -- Guilds -- */

	public function getGuildDetails($guildName = null, $guildId = null) {
		if($guildName) $guildName = urlencode($guildName);
		return $this->getData($this->urls['guilddetails'] . ($guildName || $guildId ? '?' : '') . ($guildName ? 'guild_name=' . $guildName . ($guildId ? '&' : ''): '') . ($guildId ? 'guild_id=' . $guildId : ''));
	}
}


/* -- Crap -- */
$api = new Api();
$api->debug = true;
$api->lang = 'en';
$api->getBuildId('adverse to fun');